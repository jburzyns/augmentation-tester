# Augmentation Tester

This is a dummy package for studies related to event augmentation in AnalysisBase

## Setting up

Clone the repository:
```
git clone ssh://git@gitlab.cern.ch:7999/jburzyns/augmentation-tester.git
```
Make the build directory and configure a release:
```
mkdir build; cd build;
asetup AnalysisBase,master,latest
```
Next, compile the code using 
```
cmake ../augmentation-tester
make
```

## Running

Navigate to the `run` directory, and run over your augmented derivation
```
cd $TestArea/../run
athena AugmentationTester/AugmentationTesterJobOptions.py --filesInput=/path/to/your/DAOD/file.pool.root --evtMax=10 
```
