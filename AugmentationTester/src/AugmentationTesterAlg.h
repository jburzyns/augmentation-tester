#ifndef AugmentationTester_AugmentationTesterAlg_H
#define AugmentationTester_AugmentationTesterAlg_H

// Athena includes
#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "StoreGate/ReadHandleKey.h"

// ROOT includes
#include "TTree.h"
#include "TH1D.h"
#include "TH2D.h"

// xAOD includes
#include "xAODEgamma/ElectronContainer.h"

// standard includes
#include <string>
#include <map>

class AugmentationTesterAlg: public ::AthAnalysisAlgorithm { 
 public: 
  AugmentationTesterAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~AugmentationTesterAlg(); 

  virtual StatusCode  initialize();     
  virtual StatusCode  beginInputFile(); 
  virtual StatusCode  execute();        
  virtual StatusCode  finalize();       
  
 private: 
   // properties
   SG::ReadHandleKey<xAOD::ElectronContainer> m_lrtElectronContainer_SGKey 
      { this, "LRTElectronContainerName", "LRTElectrons", "" };

   // member vars
   int m_eventCount = 0;

   // histograms
   TH1F* m_nLRTElectrons;
}; 

#endif //> !AugmentationTester_AugmentationTesterAlg_H
