// AugmentationTester includes
#include "AugmentationTesterAlg.h"

#include "AthenaKernel/Units.h"

#include "xAODEventInfo/EventInfo.h"

// ROOT includes
#include "TMath.h"

AugmentationTesterAlg::AugmentationTesterAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){}

AugmentationTesterAlg::~AugmentationTesterAlg() {}

StatusCode AugmentationTesterAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  // ReadHandles
  ATH_CHECK(m_lrtElectronContainer_SGKey.initialize());

  // Histograms
  m_nLRTElectrons = new TH1F("nLRTElectrons", "nLRTElectrons", 10, 0, 10);
  ATH_CHECK( histSvc()->regHist("/PLOTSTREAM/nLRTElectrons", m_nLRTElectrons));  

  return StatusCode::SUCCESS;
}

StatusCode AugmentationTesterAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  return StatusCode::SUCCESS;
}

StatusCode AugmentationTesterAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  // retrieve the TruthJetContainer object from the store
  SG::ReadHandle<xAOD::ElectronContainer> electronContainerRH(m_lrtElectronContainer_SGKey);

  if ( ! electronContainerRH.isValid() ) {
    ATH_MSG_ERROR("No electron container" <<  m_lrtElectronContainer_SGKey << " found in evtStore");
    return StatusCode::FAILURE;
  }

  int nLRTElectrons = 0;
  for (xAOD::ElectronContainer::const_iterator itr=electronContainerRH->begin(); itr != electronContainerRH->end(); ++itr) {
    (void) itr;
    nLRTElectrons++;
  }

  // fill event-level histograms
  m_nLRTElectrons->Fill(nLRTElectrons);

  return StatusCode::SUCCESS;
}

StatusCode AugmentationTesterAlg::beginInputFile() { 
  return StatusCode::SUCCESS;
}


