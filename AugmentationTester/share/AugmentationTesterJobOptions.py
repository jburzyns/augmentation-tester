jps.AthenaCommonFlags.AccessMode = "ClassAccess" 

# set up the output stream
jps.AthenaCommonFlags.HistOutputs = ["PLOTSTREAM:AugmentationHists.root"]  

# add alg to the sequence
athAlgSeq += CfgMgr.AugmentationTesterAlg(LRTElectronContainerName = "LRTElectrons")

include("AthAnalysisBaseComps/SuppressLogging.py")              
